<?php
declare(strict_types = 1);

namespace FizzBuzz;

class FizzBuzz
{
    const MULTIPLE_THREE = 'Fizz';
    const MULTIPLE_FIVE = 'Buzz';

    /**
     * @param int $from
     * @param int $to
     *
     * @return \Generator
     */
    public function getGenerator(int $from, int $to): \Generator
    {
        if (!$this->isInputValuesValid($from, $to)) {
            throw new \InvalidArgumentException('Arguments to FuzzBuzz generator are invalid');
        }
        for ($i = $from; $i <= $to; ++$i) {
            if ($i === 0) {
                yield $i;
                continue;
            }
            $result = '';
            if ($i % 3 === 0) {
                $result .= self::MULTIPLE_THREE;
            }
            if ($i % 5 === 0) {
                $result .= self::MULTIPLE_FIVE;
            }
            if ($result === '') {
                $result = $i;
            }

            yield $result;
        }
    }

    /**
     * @param int $from
     * @param int $to
     *
     * @return bool
     */
    protected function isInputValuesValid(int $from, int $to): bool
    {
        if (($to <=> $from) === 1) {
            return true;
        }

        return false;
    }
}
