<?php
declare(strict_types = 1);

namespace Test;

require __DIR__.'/../src/FizzBuzz.php';

use FizzBuzz\FizzBuzz;

class FizzBuzzTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @covers FizzBuzz\FizzBuzz::isInputValuesValid
     */
    public function testIsInputValuesValid()
    {
        $class = new \ReflectionClass(FizzBuzz::class);
        $method = $class->getMethod('isInputValuesValid');
        $method->setAccessible(true);
        $isValid = $method->invokeArgs(new FizzBuzz(), [34, 34]);
        $this->assertFalse($isValid);
        $isValid = $method->invokeArgs(new FizzBuzz(), [4, -1]);
        $this->assertFalse($isValid);
        $isValid = $method->invokeArgs(new FizzBuzz(), [0, 5]);
        $this->assertTrue($isValid);
    }

    /**
     * @covers  FizzBuzz\FizzBuzz::getGenerator
     */
    public function testGetGenerator()
    {
        $fizzBuzz = new FizzBuzz();
        $fizzBuzzGenerator = $fizzBuzz->getGenerator(0, 16);
        $numberIteration = -1;
        $messageTemplate = 'Position %d has a wrong value %s';
        foreach ($fizzBuzzGenerator as $value) {
            ++$numberIteration;
            switch (true) {
                case ($numberIteration === 0): {
                    $this->assertEquals(0, $value, sprintf($messageTemplate, $numberIteration, $value));
                    break;
                }
                case ($numberIteration === 1): {
                    $this->assertEquals(1, $value, sprintf($messageTemplate, $numberIteration, $value));
                    break;
                }
                case ($numberIteration === 16): {
                    $this->assertEquals(16, $value, sprintf($messageTemplate, $numberIteration, $value));
                    break;
                }
                case ($numberIteration === 15): {
                    $this->assertEquals('FizzBuzz', $value, sprintf($messageTemplate, $numberIteration, $value));
                    break;
                }
                case ($numberIteration === 10): {
                    $this->assertEquals('Buzz', $value, sprintf($messageTemplate, $numberIteration, $value));
                    break;
                }
                case ($numberIteration === 9): {
                    $this->assertEquals('Fizz', $value, sprintf($messageTemplate, $numberIteration, $value));
                    break;
                }
                case ($numberIteration === 8): {
                    $this->assertEquals(8, $value, sprintf($messageTemplate, $numberIteration, $value));
                    break;
                }
            }
        }
        $this->assertEquals(17, $numberIteration + 1, 'The count iteration is not equal 17');
    }
}
