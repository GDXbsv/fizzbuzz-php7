<?php
declare(strict_types = 1);
require __DIR__.'/src/FizzBuzz.php';

use FizzBuzz\FizzBuzz;

$fizzBuzz = new FizzBuzz();
$fizzBuzzGenerator = $fizzBuzz->getGenerator(1, 100);
foreach ($fizzBuzzGenerator as $value) {
    print_r($value . ", ");
}
